# SVG Hex Tile Game
A wet weekend, a couple of weeks back, my daughter and I 'invented' a simple board game. It's a collaborative D&D-like, discover-a-maze-kill-monsters-win-gems kind of thing. It was moderately fun to design and to play, but the paper-based version was a bit tedious when it came to locking all the maze tiles in place, and tracking the HP of each player and monster. So over the next few evenings I learnt how to build an interactive SVG version, supplied here as a working, single-file game for playing and hacking.

For more information, see https://chrismolloy.com/hextilegame

![Sample Hex Tile Game board](GameHexTile.png)

